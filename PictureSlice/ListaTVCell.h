//
//  ListaTVCell.h
//  PictureSlice
//
//  Created by Leonardo Trancoso on 24/03/15.
//  Copyright (c) 2015 Leonardo Trancoso. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListaTVCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblLista;



@end
