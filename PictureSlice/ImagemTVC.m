//
//  ImagemTVC.m
//  PictureSlice
//
//  Created by Leonardo Trancoso on 25/03/15.
//  Copyright (c) 2015 Leonardo Trancoso. All rights reserved.
//

#import "ImagemTVC.h"
#import "ImagemTVCell.h"
#import "ViewController.h"

@interface ImagemTVC ()

@end

@implementation ImagemTVC{
    NSMutableArray *imgItens;
    NSArray *imagem;
    NSArray *imagemCorreta;
    int contador;
    int validador;
    __weak IBOutlet UIBarButtonItem *validar;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self carregaImagem:self.imagemSelecionada];
    validar.enabled = NO;
    validador = 0;
    NSLog(@"%zd", self.imagemSelecionada);
    
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(slideToLeftWithGestureRecognizer:)];
    swipeLeft.direction = UISwipeGestureRecognizerDirectionRight;
    
    [self.tableView addGestureRecognizer:swipeLeft];
    
}


-(void)slideToLeftWithGestureRecognizer:(UISwipeGestureRecognizer *)gestureRecognizer{
    self.tableView.editing = YES;
    
    
}


-(void)carregaImagem:(NSInteger *)indice{
    int xyz = (int)indice;
    switch (xyz) {
        case 1:
            imgItens = [@[@"imagem01", @"imagem02", @"imagem03", @"imagem04"] mutableCopy];
            break;
        case 0:
            imgItens = [@[@"cid01.png", @"cid02.png", @"cid03.png", @"cid04.png", @"cid05.png", @"cid06.png",@"cid06.png", @"cid07.png", @"cid08.png", @"cid09.png", @"cid10.png", @"cid11.png", @"cid12.png", @"cid13.png", @"cid13.png"] mutableCopy];
            break;
        case 2:
            imgItens = [@[@"imagem01", @"imagem02", @"imagem03", @"imagem04"] mutableCopy];
            break;
        case 3:
            imgItens = [@[@"imagem01", @"imagem02", @"imagem03", @"imagem04"] mutableCopy];
            break;
        default:
            break;
    }

}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [imgItens count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ImagemTVCell *cell = [tableView dequeueReusableCellWithIdentifier:@"img" forIndexPath:indexPath];
    cell.imgSlice.image = [UIImage imageNamed:[imgItens objectAtIndex:indexPath.row]];
    
    
    return cell;
}
- (IBAction)voltar:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)validarImg:(id)sender {
    
    
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        [imgItens removeObjectAtIndex:indexPath.row];
        
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        contador++;
        if (contador == 3) {
            
            validar.enabled = YES;
        }
    }
    //else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    //}
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    imagemCorreta = @[@"cid01.png", @"cid02.png", @"cid03.png", @"cid04.png", @"cid05.png", @"cid06.png", @"cid07.png", @"cid08.png", @"cid10.png", @"cid11.png", @"cid12.png", @"cid13.png"];
    imagem = [[NSArray alloc] initWithArray:imgItens];
    
    if ([imagemCorreta isEqualToArray:imagem]) {
        validador = 1;
        NSLog(@"%i",validador);
    }else{
        validador = 0;
        NSLog(@"%i",validador);
    }
    
    ViewController *val = [segue destinationViewController];
    val.validar = validador;
}


@end
