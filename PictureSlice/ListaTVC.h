//
//  ListaTVC.h
//  PictureSlice
//
//  Created by Leonardo Trancoso on 24/03/15.
//  Copyright (c) 2015 Leonardo Trancoso. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListaTVC : UITableViewController < UISearchBarDelegate>

@property int i;
@property (strong, nonatomic) NSArray *searchResults;
@property (strong, nonatomic) NSDictionary *indices;


@end
