//
//  ListaTVC.m
//  PictureSlice
//
//  Created by Leonardo Trancoso on 24/03/15.
//  Copyright (c) 2015 Leonardo Trancoso. All rights reserved.
//

#import "ListaTVC.h"
#import "ListaTVCell.h"
#import "ImagemTVC.h"


@interface ListaTVC ()






@end

@implementation ListaTVC{
    NSArray *listaItens;
    //NSDictionary *indices;
    NSArray *indiceSections;
    NSArray *indice2;
    int procurando;
    NSString *searchTexto;
    NSArray *searchResultado;

    
}



- (void)viewDidLoad {
    [super viewDidLoad];
    procurando = 0;
    //listaItens = @[@"imagem01", @"imagem02", @"imagem03", @"imagem04"];
    
    _indices = @{@"I": @[@"imagem01"],
                @"A": @[@"imagem01"]};
    indice2 = @[@"imagem01"];
    
    indiceSections = [[_indices allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    
    
    
    
}



- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    if (procurando == 1) {
        return [NSArray arrayWithObjects:searchTexto];
    }else{
        return indiceSections;
    }
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (procurando == 1) {
        return 1;
    }else{
       return [indiceSections count];
    }

    
}



- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (procurando == 1) {
        return searchTexto;
    }else{
        return [indiceSections objectAtIndex:section];
    }
    
}





- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    
   
    if (procurando == 1) {
        return [searchResultado count];
    }else{
        NSString *sectionTitle = [indiceSections objectAtIndex:section];
        NSArray *sectionIndice = [_indices objectForKey:sectionTitle];
    
        return [sectionIndice count];
    }
    
    
    
    
    
    //return [listaItens count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    ListaTVCell *cell = [tableView dequeueReusableCellWithIdentifier:@"list" forIndexPath:indexPath];
    
    if (procurando == 1) {
        cell.lblLista.text = [searchResultado objectAtIndexedSubscript:indexPath.row];
    } else {
        NSString *sectionTitle = [indiceSections objectAtIndex:indexPath.section];
        NSArray *sectionIndice = [_indices objectForKey:sectionTitle];
        NSString *indice = [sectionIndice objectAtIndex:indexPath.row];
        
        
        
        cell.lblLista.text = indice;
    }
    
    
    
    
    
    
    
   //cell.lblLista.text = [listaItens objectAtIndex:indexPath.row];
   
    
    return cell;
}




- (int)filterContentForSearchText:(NSString*)searchText
{
    // Verify if searchText is nil or empty.
    if (searchText == nil || [searchText isEqualToString:@""])
        return 0;
    
    // This method will help me with the search.
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"indice2 contains[cd] %@", searchText];
    
    // Get the key.
    searchTexto = [NSString stringWithFormat:@"%c", [searchText characterAtIndex:0]];
    
    // Get the array related to the key.
    NSArray *filtro = [_indices objectForKey:searchTexto];
    
    // Verify if section exist.
    if ([filtro count] > 0) {
        // Apply the search predicate and return YES for success.
        searchResultado = [filtro filteredArrayUsingPredicate:resultPredicate];
        
        return 1;
    } else
        return 0;
    
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    // Close keyboard.
    [searchBar resignFirstResponder];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    // Get search texts.
    if ([self filterContentForSearchText:[searchBar text]])
        procurando = 1;
    else
        procurando = 0;
    
    [[self tableView] reloadData];
    
}



- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
   
    ImagemTVC *img = [segue destinationViewController];
    img.imagemSelecionada = [[self.tableView indexPathForSelectedRow] row];
}


@end
