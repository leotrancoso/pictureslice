//
//  ViewController.m
//  PictureSlice
//
//  Created by Leonardo Trancoso on 24/03/15.
//  Copyright (c) 2015 Leonardo Trancoso. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *resultado;
@property (weak, nonatomic) IBOutlet UILabel *lbl01;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.validar == 1) {
        _resultado.image = [UIImage imageNamed:@"cidade"];
        _lbl01.text = @"Parabéns! Imagem Correta!";
    }else{
        _resultado.image = [UIImage imageNamed:@"youlose"];
    }
}



@end
