//
//  ImagemTVCell.h
//  PictureSlice
//
//  Created by Leonardo Trancoso on 25/03/15.
//  Copyright (c) 2015 Leonardo Trancoso. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImagemTVCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgSlice;

@end
